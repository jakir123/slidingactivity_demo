package com.cse24gmail.jakir.slidingactivity_demo;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class SecondActivity extends ActionBarActivity {

    private Button btnBackToMainActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slideinleft, R.anim.slideoutright);
        setContentView(R.layout.activity_second);

        btnBackToMainActivity= (Button) findViewById(R.id.btnBack);
        btnBackToMainActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoMainActivity=new Intent(SecondActivity.this,MainActivity.class);
                gotoMainActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                gotoMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(gotoMainActivity);
                finish();
            }
        });
    }
}
